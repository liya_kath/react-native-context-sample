import { StatusBar } from "expo-status-bar";
import React, { useContext } from "react";
import { StyleSheet, Text, View } from "react-native";
import Heading from "./src/components/header/Heading";
import Task from "./src/context/taskContext";

export default function App() {
  return (
    <Task>
      <View style={styles.container}>
        <Heading />
        <StatusBar style="auto" />
      </View>
    </Task>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
});
