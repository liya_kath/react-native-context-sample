import React, { useContext } from "react";
import { Button, StyleSheet, Text, View } from "react-native";
import { TaskContext } from "../../context/taskContext";

const Heading = () => {
  const { count, incrementCounter, DecrementCounter } = useContext(TaskContext);

  return (
    <View>
      <Text>{count}</Text>
      <Button title="click here" onPress={incrementCounter} />
      <Button title="click here" onPress={DecrementCounter} />
    </View>
  );
};

export default Heading;
const styles = StyleSheet.create({});
