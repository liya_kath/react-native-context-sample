import React, { createContext, useState } from "react";
export const TaskContext = createContext();

const Task = ({ children }) => {
  const [count, setcount] = useState(10);

  const incrementCounter = () => {
    setcount((count) => count + 1);
  };
  const DecrementCounter = () => {
    setcount((count) => count - 1);
  };

  return (
    <TaskContext.Provider value={{ count, incrementCounter, DecrementCounter }}>
      {children}
    </TaskContext.Provider>
  );
};

export default Task;
